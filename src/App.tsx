import React, { useEffect, useState } from 'react';
import './App.css';
import "react-datepicker/dist/react-datepicker.css";

import 'semantic-ui-css/semantic.min.css'
import PageHeader from './components/PageHeader';
import SelectInternaOtpremnica from './components/SelectInternaOtpremnica';
import { useKeycloak } from '@react-keycloak/web';
import axios from 'axios';


const BrowserRouter = require("react-router-dom").BrowserRouter;
const Route = require("react-router-dom").Route;
const Link = require("react-router-dom").Link;
const Switch = require("react-router-dom").Switch
const Redirect = require("react-router-dom").Redirect



interface IProps {

}

interface IState {
    user: any
}


function App(props: IProps) {
    const { keycloak }: any = useKeycloak();

    
    // axios.get(`http://localhost:4000/user/${keycloak.tokenParsed?.sub}`)
    //         .then(res => {
    //             console.log('Nije puklo')
    //         })
    //         .catch(async error => {
    //             let config = {
    //                 headers: {
    //                     'Content-Type': 'application/json'
    //                 }
    //             }
    //             let data = {
    //                 userID: keycloak.parsedToken?.sub,
    //                 fullName: keycloak.parsedToken?.familyName
    //             }
    //             console.log(data);
    //             await axios.post(`http://localhost:4000/user`, data, config )
    //                 .then(res => {
    //                     console.log('nije puklo drugo')
    //                 })
    //                 .catch(error => {
    //                     console.log('greska')
    //             });
    // });
    

return (

    <BrowserRouter>
        <Switch>
            <Route exact path="/">
                <PageHeader activeItem={"InternaOtpremnica"} />
                {<SelectInternaOtpremnica keycloak={keycloak} />}
            </Route>

            {/* <Route exact  path="/InternaOtpremnica">
                    <PageHeader handleLogOut={this.handleLogOut} activeItem={"InternaOtpremnica"}/>
                    <SelectInternaOtpremnica/>
                </Route>
                <Route exact  path="/StavkaInterneOtpremnice">
                    <PageHeader handleLogOut={this.handleLogOut} activeItem={"StavkaInterneOtpremnice"}/>
                    <SelectStavkaInterneOtpremnice/>
                </Route>
                <Route exact  path="/kalkulacijaCene">
                    <PageHeader handleLogOut={this.handleLogOut} activeItem={"kalkulacijaCene"}/>
                    <SelectKalkulacijaCene/>
                </Route> */}
        </Switch>
    </BrowserRouter>
)
  
}

export default App;