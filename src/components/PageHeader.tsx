import React, { Component, useEffect } from 'react'
import {Button, Menu} from 'semantic-ui-react'
import { useKeycloak } from '@react-keycloak/web';
const Link = require("react-router-dom").Link
const useHistory = require("react-router-dom").useHistory



interface IProps {
    activeItem: string
}

interface IState {

}


function PageHeader(props:IProps)  {

     let history = useHistory();
     const { keycloak }: any = useKeycloak();
     

     return (

            <Menu>
                <Menu.Item
                    name='Interna Otpremnica'
                    active={props.activeItem === 'InternaOtpremnica'}
                    onClick ={() => {
                        history.push('/InternaOtpremnica')
                    }}
                >
                    Interna Otpremnica
                </Menu.Item>
                <Menu.Item
                    position = 'right'
                    name='logout'
                    active={props.activeItem === ''}
                    onClick ={() => {
                        if (keycloak)
                         window.location.href = keycloak.createLogoutUrl();
                    }}
                >
                    LogOut
                </Menu.Item>
        </Menu>
    )

}

export default PageHeader;