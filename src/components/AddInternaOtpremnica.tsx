import React from 'react';
//@ts-ignore
import DatePicker from "react-datepicker";
import { Button, Dropdown, Form, Grid, Input, Modal, Table } from "semantic-ui-react";
import axios from 'axios'
import AddStavkaInternaOtpremnica from './AddStavkaInterneOtpremnice';



interface JM {
    sifraJediniceMere: number,
    naziv: string
}

interface Proizvod {
    sifraProizvoda: number,
    cena: number,
    naziv: string,
    jedinica_mere: JM
}

interface Pdv {
    sifraPDV: number,
    iznosPDV: number
}

interface Stavka {
    sifra_otpremnice: number,
    redni_broj: number,
    rabat: number,
    kolicina: number,
    proizvod: Proizvod,
    pdv: Pdv,
    operation: string
}

interface Otpremnica {
    SifraOtpremnice: number;
    datumDokumenta: Date;
    maloprodaja: string,
    skladiste: string,
    stavke: Stavka[]
}

interface IProps {
    closeModal: any,
    activeOtpremnica: Otpremnica | undefined
}

interface DropDownItem {
    key: number,
    value: number,
    text: string
}
interface IState {
    sifraOtpremnice: number,
    datumDokumenta: any,
    activeItem: number,
    stavke: Stavka[],
    skladiste: string,
    maloprodaja: string,
    modalAddOpen: boolean,
    modalUpdateOpen: boolean,
}


class AddInternaOtpremnica extends React.Component<IProps, IState> {

    constructor(props: IProps) {
        super(props);
        this.state = {
            modalAddOpen: false,
            modalUpdateOpen: false,
            stavke: [],
            activeItem: 0,
            sifraOtpremnice: 0,
            skladiste: '',
            maloprodaja: '',
            datumDokumenta: new Date(),
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleClick = this.handleClick.bind(this);
        this.handleSubmitAddOtpremnica = this.handleSubmitAddOtpremnica.bind(this);
    }

    handleClick = (id: number) => {
        this.setState({
            activeItem: id
        });
    }



    handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({
            ...this.state,
            [e.target.name]: e.target.value
        });
    }

    private handleSubmitAddOtpremnica = async (): Promise<void> => {
        let config = {
            headers: {
                'Content-Type': 'application/json'
            }
        }

        let stavkeZaSlanje = [];

        for (let stavka of this.state.stavke) {
            let s = {
                kolicina: parseInt(stavka.kolicina.toString()),
                operation: stavka.operation,
                pdv: stavka.pdv.sifraPDV,
                proizvod: stavka.proizvod.sifraProizvoda,
                rabat: parseInt(stavka.rabat.toString()),
                redni_broj: parseInt(stavka.redni_broj.toString()),
                sifra_otpremnice: stavka.sifra_otpremnice
            }
            stavkeZaSlanje.push(s);
        }

        let data = {
            datumDokumenta: this.state.datumDokumenta,
            SifraOtpremnice: parseInt(this.state.sifraOtpremnice.toString()),
            maloprodaja: this.state.maloprodaja,
            skladiste: this.state.skladiste,
            stavke: stavkeZaSlanje
        }

        console.log(data);


        await axios.post('http://localhost:4000/otpremnica', data, config)
            .then(async res => {
                alert("Opremnica uspesno sacuvana!")
                let o = await axios.get(`http://localhost:4000/otpremnica/${data.SifraOtpremnice}`)
                this.props.closeModal(o.data);
            })
            .catch(error => {
                if (error.response) {
                    console.log(error.response.data)
                    alert(error.response.data.message[0]);
                }
            })

    }

    handleUpdateStavka = async (s: Stavka) => {
        console.log(s);
        await this.setState({
            modalUpdateOpen: false,
            stavke: this.state.stavke.filter(obj => obj.redni_broj !== s.redni_broj)
        })
        await this.setState({
            stavke: [...this.state.stavke, s]
        })
    }

    handleAddStavka1 = async (s: Stavka) => {
        if (this.state.stavke.filter(obj => obj.proizvod.sifraProizvoda === s.proizvod.sifraProizvoda).length !== 0) {
            let stavka = this.state.stavke.filter(obj => obj.proizvod.sifraProizvoda === s.proizvod.sifraProizvoda)[0];
            stavka.kolicina = parseInt(stavka.kolicina.toString()) + parseInt(s.kolicina.toString());
            stavka.pdv = s.pdv;
            stavka.rabat = s.rabat;
            s.operation = 'U'
            await this.setState({
                modalAddOpen: false
            })
        }
        else {
            await this.setState({
                modalAddOpen: false,
                stavke: [...this.state.stavke, s]
            })
        }
    }

    handleAddStavka = (s: Stavka) => {
        this.setState({
            modalAddOpen: false,
            stavke: [...this.state.stavke, s]
        })
    }


    async componentDidMount() {

        if (this.props.activeOtpremnica) {
            this.setState({
                sifraOtpremnice: this.props.activeOtpremnica.SifraOtpremnice,
                datumDokumenta: new Date(this.props.activeOtpremnica.datumDokumenta),
                skladiste: this.props.activeOtpremnica.skladiste,
                maloprodaja: this.props.activeOtpremnica.maloprodaja,
                stavke: this.props.activeOtpremnica.stavke
            })
        }


    }



    render() {

        return (
            <div>
                <Grid columns={2} divided padded>
                    <Grid.Row>
                        <div id='otpremnicaForm'>
                            <Form id='AddOtpremnicaForm'>
                                <Form.Field>
                                    <label>Sifra Otpremnice</label>
                                    <Input disabled={this.props.activeOtpremnica ? true : false} icon='user outline' iconPosition='left' placeholder='SifraOtpremnice' name='sifraOtpremnice' value={this.state.sifraOtpremnice} onChange={this.handleChange} />
                                </Form.Field>


                                <Form.Field>
                                    <label>Skladiste</label>
                                    <Input icon='user outline' iconPosition='left' placeholder='skladiste' name='skladiste' value={this.state.skladiste} onChange={this.handleChange} />
                                </Form.Field>
                                <Form.Field>
                                    <label>Maloprodaja</label>
                                    <Input icon='user outline' iconPosition='left' placeholder='maloprodaja' name='maloprodaja' value={this.state.maloprodaja} onChange={this.handleChange} />
                                </Form.Field>

                                <Form.Field>
                                    <label>Datum dokumenta</label>
                                    <DatePicker
                                        dateFormat='dd-MM-yyyy'
                                        selected={this.state.datumDokumenta}
                                        onChange={(date: any) => this.setState({ datumDokumenta: date })}
                                        placeholderText="Click to select date..."
                                        maxDate={new Date()}
                                    />
                                </Form.Field>
                            </Form>
                        </div>

                        <div id='stavkaTable'>
                            <Table celled selectable>
                                <Table.Header>
                                    <Table.Row>
                                        <Table.HeaderCell>Redni broj</Table.HeaderCell>
                                        <Table.HeaderCell>Proizvod</Table.HeaderCell>
                                        <Table.HeaderCell>Kolicina</Table.HeaderCell>
                                        <Table.HeaderCell>Osnovna cena</Table.HeaderCell>
                                        <Table.HeaderCell>PDV</Table.HeaderCell>
                                        <Table.HeaderCell>Rabat</Table.HeaderCell>
                                        <Table.HeaderCell>Ukupno</Table.HeaderCell>
                                    </Table.Row>
                                </Table.Header>

                                <Table.Body>
                                    {this.state.stavke.filter(e => e.operation !== 'D').map((element, index) => (

                                        <Table.Row key={element.redni_broj} active={this.state.activeItem == element.redni_broj}
                                            onClick={() => {
                                                this.handleClick(element.redni_broj)

                                            }}>
                                            <Table.Cell>{index + 1}</Table.Cell>
                                            <Table.Cell>{element.proizvod.naziv}</Table.Cell>
                                            <Table.Cell>{element.kolicina}</Table.Cell>
                                            <Table.Cell>{element.proizvod.cena}</Table.Cell>
                                            <Table.Cell>{element.pdv.iznosPDV}</Table.Cell>
                                            <Table.Cell>{element.rabat}</Table.Cell>
                                            <Table.Cell>{dajUkupno(element).toFixed(2)}</Table.Cell>
                                        </Table.Row>


                                    ))}

                                </Table.Body>
                            </Table>
                        </div >
                    </Grid.Row>
                    <Grid.Row>
                    </Grid.Row>

                </Grid>
                <Button primary type='submit' onClick={() => this.handleSubmitAddOtpremnica()}>Submit</Button>
                <Button.Group color='grey' floated="right">
                    <Button secondary class='secButtons' onClick={() => { this.setState({ modalAddOpen: true }) }}>DodajStavku</Button>
                    <Button disabled={this.state.activeItem === 0} onClick={() => {
                        let selectedStavka = this.state.stavke.filter(e => e.redni_broj === this.state.activeItem)[0];
                        if (selectedStavka.operation === 'I')
                            this.setState({
                                stavke: this.state.stavke.filter(e => e.redni_broj !== this.state.activeItem)
                            })
                        else {
                            this.state.stavke.filter(e => e.redni_broj === this.state.activeItem)[0].operation = 'D'
                        }
                        this.setState({
                            activeItem: 0
                        })
                    }} secondary class='secButtons'>ObrisiStavku</Button>
                    <Button disabled={this.state.activeItem === 0} onClick={() => { this.setState({ modalUpdateOpen: true }) }} secondary class='secButtons'>IzmeniStavku</Button>
                </Button.Group>
                <Modal
                    open={this.state.modalAddOpen}
                    onClose={() => this.setState({
                        modalAddOpen: false
                    })}
                    closeIcon>
                    <Modal.Header>Dodaj otpremnicu</Modal.Header>
                    <Modal.Content>
                        <AddStavkaInternaOtpremnica sifraOtpremnice={this.state.sifraOtpremnice} rb={this.state.stavke[this.state.stavke.length-1]?this.state.stavke[this.state.stavke.length-1].redni_broj+1 : 1 } activeStavka={undefined} closeModal={this.handleAddStavka1} />
                    </Modal.Content>
                </Modal>
                <Modal
                    open={this.state.modalUpdateOpen}
                    onClose={() => this.setState({
                        modalUpdateOpen: false
                    })}
                    closeIcon>
                    <Modal.Header>Update otpremnica</Modal.Header>
                    <Modal.Content>
                        <AddStavkaInternaOtpremnica sifraOtpremnice={this.state.sifraOtpremnice} rb={this.state.stavke.length + 1} activeStavka={this.state.stavke.find(t => t.redni_broj === this.state.activeItem)} closeModal={this.handleUpdateStavka} />
                    </Modal.Content>
                </Modal>
            </div>
        );
    }
}

export default AddInternaOtpremnica;

function dajUkupno(element: Stavka): number {
    return element.kolicina * (element.proizvod.cena * ((100 + element.pdv.iznosPDV) / 100) * ((100 - element.rabat) / 100))
}
