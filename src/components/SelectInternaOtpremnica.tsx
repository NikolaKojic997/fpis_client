import axios from 'axios';
import React from 'react';
// @ts-ignore
// import DatePicker from "react-datepicker";
import { Button, Form, Input, Modal, Table } from "semantic-ui-react";
import date from 'date-and-time'
import AddInternaOtpremnica from './AddInternaOtpremnica';
import { useKeycloak } from '@react-keycloak/web';
import { keycloak } from '../keycloak';
var jwt = require('jsonwebtoken');

interface JM {
    sifraJediniceMere: number,
    naziv: string
}

interface Proizvod {
    sifraProizvoda: number,
    cena: number,
    naziv: string,
    jedinica_mere: JM
}

interface Pdv {
    sifraPDV: number,
    iznosPDV: number
}

interface Stavka {
    sifra_otpremnice: number,
    redni_broj: number,
    rabat: number,
    kolicina: number,
    proizvod: Proizvod,
    pdv: Pdv,
    operation: string
}

interface Otpremnica {
    SifraOtpremnice: number;
    datumDokumenta: Date;
    maloprodaja: string,
    skladiste: string,
    stavke: Stavka[]
}

interface IProps {
    keycloak: any
}

interface IState {
    otpremnice: Otpremnica[],
    activeItem: number,
    modalAddOpen: boolean,
    modalUpdateOpen: boolean

}



class SelectInternaOtpremnica extends React.Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);
        this.state = {
            otpremnice: [],
            activeItem: 0,
            modalAddOpen: false,
            modalUpdateOpen: false
        }
        this.handleClick = this.handleClick.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
        this.handleUpdateOtpremnica = this.handleUpdateOtpremnica.bind(this);
        this.handleAddOtpremnica = this.handleAddOtpremnica.bind(this);
    }

    async componentDidMount() {

        // const { keycloak }: any = useKeycloak();


        setTimeout(async() => {
            console.log(jwt.sign({id: `${keycloak.tokenParsed?.sub}`}, 'mySecret'))
            const config = {
                headers: {
                    Authorization: `Bearer ${jwt.sign({id: `${keycloak.tokenParsed?.sub}` }, 'mySecret')}`,
                },
            };
    
            await axios.get('http://localhost:4000/otpremnica', config)
                .then(res => {
                    console.log(res.data)
                    this.setState({
                        otpremnice: res.data
                    })
                })
                .catch(error => {
                    console.log(error)
                });
        }, 500)
      
        
        }
        
    
    

    handleClick = (id: number) => {
        this.setState({
            activeItem: id
        });
    }


    handleAddOtpremnica = (o: Otpremnica) => {
        if (this.state.otpremnice.filter(e => e.SifraOtpremnice === o.SifraOtpremnice).length !== 0) {
            let otpremnica = this.state.otpremnice.filter(e => e.SifraOtpremnice === o.SifraOtpremnice)[0];
            otpremnica.datumDokumenta = new Date(o.datumDokumenta)
            otpremnica.maloprodaja = o.maloprodaja;
            otpremnica.skladiste = o.skladiste;
            otpremnica.stavke = [...o.stavke];
        }
        else {
            this.setState({
                modalAddOpen: false,
                otpremnice: [...this.state.otpremnice, o]
            })
        }
    }
    handleUpdateOtpremnica = (o: Otpremnica) => {
        this.setState({
            modalUpdateOpen: false,
            otpremnice: this.state.otpremnice.filter(obj => obj.SifraOtpremnice !== o.SifraOtpremnice)
        })
        this.setState({
            otpremnice: [...this.state.otpremnice, o]
        })
    }

    handleDelete = async () => {
        let activeItem = this.state.activeItem;
        await axios.delete(`http://localhost:4000/otpremnica/${activeItem}`)
            .then(res => {
                console.log(res);
                alert("Otpremnica obrisana")
            })
            .catch(err => {
                alert(err.message)
            })
        this.setState(
            {
                otpremnice: this.state.otpremnice.filter(obj => obj.SifraOtpremnice !== activeItem),
                activeItem: 0
            }
        )
    }


    render() {

        return (
            <div>
                <Table celled selectable>
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell>Sifra otpremnice</Table.HeaderCell>
                            <Table.HeaderCell>Datum dokumenta</Table.HeaderCell>
                            <Table.HeaderCell>Skladiste</Table.HeaderCell>
                            <Table.HeaderCell>Maloprodaje</Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>

                    <Table.Body>
                        {this.state.otpremnice.map(element => (

                            <Table.Row key={element.SifraOtpremnice} active={this.state.activeItem == element.SifraOtpremnice}
                                onClick={() => {
                                    this.handleClick(element.SifraOtpremnice)
                                    console.log(this.state.activeItem)
                                    console.log(this.state.otpremnice.find(t => t.SifraOtpremnice === this.state.activeItem))
                                }}>
                                <Table.Cell>{element.SifraOtpremnice}</Table.Cell>
                                <Table.Cell>{date.format(new Date(element.datumDokumenta), 'DD-MMM-YYYY')}</Table.Cell>
                                <Table.Cell>{element.skladiste}</Table.Cell>
                                <Table.Cell>{element.maloprodaja}</Table.Cell>
                            </Table.Row>


                        ))}

                    </Table.Body>
                </Table>
                <div>
                    <Button onClick={() => this.setState({ modalAddOpen: true })}>Add new</Button>
                    <Button disabled={this.state.activeItem === 0} onClick={() => this.setState({ modalUpdateOpen: true })}>Update</Button>
                    <Button disabled={this.state.activeItem === 0} onClick={this.handleDelete}>Delete</Button>
                </div>
                <Modal
                    open={this.state.modalAddOpen}
                    onClose={() => this.setState({
                        modalAddOpen: false
                    })}
                    closeIcon>
                    <Modal.Header>Dodaj otpremnicu</Modal.Header>
                    <Modal.Content>
                        <AddInternaOtpremnica activeOtpremnica={undefined} closeModal={this.handleAddOtpremnica} />
                    </Modal.Content>
                </Modal>
                <Modal
                    open={this.state.modalUpdateOpen}
                    onClose={() => this.setState({
                        modalUpdateOpen: false
                    })}
                    closeIcon>
                    <Modal.Header>Update otpremnica</Modal.Header>
                    <Modal.Content>
                        <AddInternaOtpremnica activeOtpremnica={this.state.otpremnice.find(t => t.SifraOtpremnice === this.state.activeItem)} closeModal={this.handleUpdateOtpremnica} />
                    </Modal.Content>
                </Modal>
            </div>
        );
    }
}

export default SelectInternaOtpremnica;