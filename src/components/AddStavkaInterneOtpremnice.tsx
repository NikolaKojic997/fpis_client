import React from 'react';
//@ts-ignore
import DatePicker from "react-datepicker";
import { Button, Dropdown, Form, Input } from "semantic-ui-react";
import axios from 'axios'

interface JM {
    sifraJediniceMere: number,
    naziv: string
}

interface Proizvod {
    sifraProizvoda: number,
    cena: number,
    naziv: string,
    jedinica_mere: JM
}

interface Pdv {
    sifraPDV: number,
    iznosPDV: number
}

interface Stavka {
    sifra_otpremnice: number,
    redni_broj: number,
    rabat: number,
    kolicina: number,
    proizvod: Proizvod
    pdv: Pdv,
    operation: string
}


interface IProps {
    closeModal: any,
    activeStavka: Stavka | undefined,
    rb: number,
    sifraOtpremnice: number
}

interface DropDownItem {
    key: number,
    value: number,
    text: string
}
interface IState {
    redniBroj: number
    sifraOtpremnice: number,
    datumDokumenta: any,
    skladiste: string,
    maloprodaja: string,
    nizProizvoda: Proizvod[],
    proizvodi: DropDownItem[],
    selectedProizvod: number,
    kolicina: number,
    pdv:DropDownItem [],
    nizPDV: Pdv[]
    selectedPDV: number,
    rabat: number
}




class AddStavkaInternaOtpremnica extends React.Component<IProps, IState> {

    constructor(props: IProps) {
        super(props);
        this.state = {
            nizPDV: [],
            nizProizvoda:[],
            rabat: 0,
            selectedPDV: 0,
            pdv: [],
            kolicina: 1,
            redniBroj: 1,
            proizvodi: [],
            sifraOtpremnice:0,
            datumDokumenta: new Date(),
            selectedProizvod: 0,
            maloprodaja: '',
            skladiste: ''
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmitAddStavka = this.handleSubmitAddStavka.bind(this);


    }

    dropdownChangeProizvod = (event: React.SyntheticEvent<HTMLElement>, data: any) => {
        this.setState({ selectedProizvod: data.value })
    }

    dropdownChangePDV = (event: React.SyntheticEvent<HTMLElement>, data: any) => {
        this.setState({ selectedPDV: data.value })
    }


    handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({
            ...this.state,
            [e.target.name]: e.target.value
        });
    }

    private handleSubmitAddStavka = async (
        e: React.FormEvent<HTMLFormElement>
    ): Promise<void> => {
        let data: Stavka = {
            kolicina: this.state.kolicina,
            rabat: this.state.rabat,
            pdv: this.state.nizPDV.filter(e => e.sifraPDV === this.state.selectedPDV)[0],
            proizvod: this.state.nizProizvoda.filter(e=> e.sifraProizvoda === this.state.selectedProizvod)[0],
            redni_broj: this.state.redniBroj,
            sifra_otpremnice: this.state.sifraOtpremnice,
            operation: 'I'
            
        }
        if(this.props.activeStavka && this.props.activeStavka.operation !== 'I')
            data.operation = 'U'
        
        this.props.closeModal(data);
};



    async componentDidMount() {

        this.setState({
            redniBroj: this.props.rb,
            sifraOtpremnice: this.props.sifraOtpremnice
        })

        if(this.props.activeStavka){
            this.setState({
                redniBroj: this.props.activeStavka.redni_broj,
                selectedProizvod: this.props.activeStavka.proizvod.sifraProizvoda? this.props.activeStavka.proizvod.sifraProizvoda: 0 ,
                kolicina: this.props.activeStavka.kolicina,
                rabat: this.props.activeStavka.rabat?this.props.activeStavka.rabat: 0,
                selectedPDV: this.props.activeStavka.pdv.sifraPDV? this.props.activeStavka.pdv.sifraPDV: 0,
                sifraOtpremnice: this.props.activeStavka.sifra_otpremnice
            })
        }

        await axios.get('http://localhost:4000/proizvod')
            .then(res => {
                console.log(res.data)
                this.setState({
                    proizvodi: res.data.map((element: Proizvod) =>
                    ({
                        value: element.sifraProizvoda,
                        text: element.naziv,
                        key: element.sifraProizvoda
                    })),
                    nizProizvoda: res.data
                })
            })
            .catch(error => {
                console.log(error)
            });
        await axios.get('http://localhost:4000/pdv')
            .then(res => {
                console.log(res.data)
                this.setState({
                    pdv: res.data.map((element: Pdv) =>
                    ({
                        value: element.sifraPDV,
                        text: element.iznosPDV,
                        key: element.sifraPDV
                    })),
                    nizPDV: res.data
                })
            })
            .catch(error => {
                console.log(error)
            });
     }



    render() {

        return (
            <div>
                <Form id='AddStavkaForm' onSubmit={this.handleSubmitAddStavka}>
                    <Form.Field>
                        <label>Redni broj</label>
                        <Input disabled = {true} icon='user outline' iconPosition='left' placeholder='RedniBroj' name='redniBroj' value={this.state.redniBroj} onChange={this.handleChange} />
                    </Form.Field>
                    <Form.Field>
                        <label>Proizvod</label>
                        <Dropdown
                            placeholder='izaberite proizvod..'
                            fluid
                            selection
                            options={this.state.proizvodi}
                            name="value"
                            value={this.state.selectedProizvod}
                            onChange={this.dropdownChangeProizvod}
                        >
                        </Dropdown>
                    </Form.Field>

                    <Form.Field>
                        <label>Kolicina</label>
                        <Input icon='user outline' iconPosition='left' placeholder='Kolicina' name='kolicina' value={this.state.kolicina} onChange={this.handleChange} />
                    </Form.Field>
                    <Form.Field>
                        <label>PDV</label>
                        <Dropdown
                            placeholder='izaberite pdv..'
                            fluid
                            selection
                            options={this.state.pdv}
                            name="value"
                            value={this.state.selectedPDV}
                            onChange={this.dropdownChangePDV}
                        >
                        </Dropdown>
                    </Form.Field>

                    <Form.Field>
                        <label>Rabat</label>
                        <Input icon='user outline' iconPosition='left' placeholder='Rabat' name='rabat' value={this.state.rabat} onChange={this.handleChange} />
                    </Form.Field>
                    
                    <Button id="loginBtn" primary type='submit'>Submit</Button>
                </Form>

            </div >
        );
    }
}

export default AddStavkaInternaOtpremnica;